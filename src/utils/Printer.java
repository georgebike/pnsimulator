package utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class Printer {
    private static Printer single_instance = null;
    private BufferedWriter bfWriter;

    private Printer(Path path){
        try {
            this.bfWriter = new BufferedWriter(new FileWriter(path.toString()));
            this.bfWriter.write("Petri Net Simulation Result \n");
            System.out.println("WRITTEN TO FILE");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Printer getInstance(Path path)
    {
        if (single_instance == null)
            single_instance = new Printer(path);

        return single_instance;
    }

    public void write(Integer currentTick, ArrayList<Integer> currentMark){
        try {
            this.bfWriter.append("T").append(String.valueOf(currentTick)).append(" : \t");
            for (Integer tokens : currentMark) {
                this.bfWriter.append(String.valueOf(tokens)).append("\t");
            }
            this.bfWriter.append("\n");
        } catch (IOException e) { e.printStackTrace(); }
    }

    public void endWriting() {
        try {
            this.bfWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
