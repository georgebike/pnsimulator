package utils;

import com.google.gson.Gson;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import models.Transition;
import models.Location;
import org.json.simple.parser.ParseException;


public class Loader {
    private static Loader single_instance = null;
    private JSONObject jsonFileObject;

    private Loader(Path path) {
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(path.toString()));
            this.jsonFileObject = (JSONObject) obj;
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    public static Loader getInstance(Path path){
        if (single_instance == null)
            single_instance = new Loader(path);

        return single_instance;
    }

    public ArrayList<Location> getLocations() {
        ArrayList<Location> locationList = new ArrayList<>();
        Gson gson = new Gson();
        JSONArray locationJsonList = (JSONArray) this.jsonFileObject.get("locations");

        // Foreach JSON location in JSONArray create a Location instance and append it to the locationList
        for (Object location : locationJsonList) {
            try {
                locationList.add(gson.fromJson(location.toString(), Location.class));
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        }
        return locationList;
    }

    public ArrayList<Transition> getTransitions(ArrayList<Location> locationList) {
        ArrayList<Transition> transitionList = new ArrayList<>();
        JSONArray transitionJsonList = (JSONArray) this.jsonFileObject.get("transitions");

        JSONArray durationIntervalJsonArray;
        // Foreach JSON location in JSONArray create a Location instance and append it to the locationList
        for (Object transition : transitionJsonList) {
            ArrayList<Location> prevLocationList = new ArrayList<>();
            ArrayList<Location> nextLocationList = new ArrayList<>();
            if (transition instanceof JSONObject) {

                // Implement the prevLocationList
                for (Object prevLocation : (JSONArray) ((JSONObject) transition).get("prevLocationList")){
                    prevLocationList.add( (Location) locationList.stream().filter(s -> prevLocation.toString().equals(s.getUuid())).findFirst().get());
                }

                // Implement the nextLocationList
                for (Object nextLocation : (JSONArray) ((JSONObject) transition).get("nextLocationList")){
                    nextLocationList.add( (Location) locationList.stream().filter(s -> nextLocation.toString().equals(s.getUuid())).findFirst().get() );
                }
                try {
                    durationIntervalJsonArray = (JSONArray) ((JSONObject) transition).get("durationIntervalList");
                    ArrayList<Integer> durationInterval = new ArrayList<>();
                    for(int i=0; i<durationIntervalJsonArray.size(); i++){
                        durationInterval.add( ((Number) durationIntervalJsonArray.get(i)).intValue() );
                    }

                    transitionList.add(new Transition(
                            (String) ((JSONObject) transition).get("uuid"),
                            durationInterval,
                            prevLocationList,
                            nextLocationList
                            ));
                } catch (Exception e) {e.printStackTrace();}
            }
        }
        return transitionList;
    }



}
