package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Transition {
    private final String uuid;
    private List<Integer> durationIntervalList;
    private List<Location> nextLocationList;
    private List<Location> prevLocationList;

    private boolean busy;         // Is busy when it's holding a token for the duration
    private int currentDuration;

    public Transition(String uuid, ArrayList<Integer> durationIntervalList, ArrayList<Location> prevLocationList, ArrayList<Location> nextLocationList) {
        this.uuid = uuid;
        this.busy = false;
        this.durationIntervalList = durationIntervalList;
        this.prevLocationList = prevLocationList;
        this.nextLocationList = nextLocationList;

        this.currentDuration = 0;

        Collections.sort(this.durationIntervalList);
    }

    public String getUuid(){
        return this.uuid;
    }

    public boolean getIsBusy(){
        return this.busy;
    }
    public void setIsBusy() { this.busy = true; }
    public void resetIsBusy() { this.busy = false; }

    public List<Location> getNextLocationList() {
        return this.nextLocationList;
    }
    public List<Location> getPrevLocationList() {
        return this.prevLocationList;
    }

    public int getCurrentDuration() {
        return this.currentDuration;
    }

    public void setNewDuration() {
        if(this.durationIntervalList.size() == 1){
            this.currentDuration = durationIntervalList.get(0);
        } else {
            this.currentDuration = (int) (Math.random() * ((durationIntervalList.get(1) - durationIntervalList.get(0)) + 1)) + durationIntervalList.get(0);
        }
    }

    public void countDown() {
        this.currentDuration -= 1;
    }
}
