package models;

public class Location {
    private final String uuid;
    private int tokens;

    public Location(String uuid, int tokens){
        this.uuid = uuid;
        this.tokens = tokens;
    }

    public String getUuid(){
        return this.uuid;
    }

    public int getTokens(){
        return this.tokens;
    }

    public void setTokens(int newTokens){
        this.tokens = newTokens;
    }
}
