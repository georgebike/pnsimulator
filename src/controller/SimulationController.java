package controller;

import models.Location;
import models.Transition;
import utils.Printer;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class SimulationController {
    private int simulationTime = -1;
    private ArrayList<Location> locationList;
    private ArrayList<Transition> transitionList;
    private Printer printer;

    public SimulationController(int simulationTime, ArrayList<Location> locationList, ArrayList<Transition> transitionList, Printer printer){
        /* Constructor that allows chosing a simulation time */
        this.simulationTime = simulationTime;
        this.locationList = locationList;
        this.transitionList = transitionList;
        this.printer = printer;
    }

    public SimulationController(ArrayList<Location> locationList, ArrayList<Transition> transitionList, Printer printer){
        /* Constuctor that allows simulation over an infinite period of time */
        this.locationList = locationList;
        this.transitionList = transitionList;
        this.printer = printer;
    }

    public void commenceSimulation() {
        int currentTick = 0;

        print(currentTick, getCurrentMark());
        printer.write(currentTick, getCurrentMark());

        if (simulationTime > 0) {
            while(currentTick < simulationTime) {
                if (!performStep()) return;
                currentTick += 1;
                printer.write(currentTick, getCurrentMark());
                print(currentTick, getCurrentMark());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } else {
            while(true) {
                if (!performStep()) return;
                currentTick += 1;
                printer.write(currentTick, getCurrentMark());
                print(currentTick, getCurrentMark());
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean performStep() {
        int notExecutable = 0;
        ArrayList<Transition> executableTransitionList = new ArrayList<>();
        ArrayList<Transition> endableTransitionList = new ArrayList<>();

        for(Transition transition: this.transitionList) {
            if(transitionAvailable(transition)){
                // Each step check all transitions and append the executable ones to the list
                System.out.print("Transition " + transition.getUuid() + " executable ");
                System.out.println(transition.getCurrentDuration());

                executableTransitionList.add(transition);
            } else if(transition.getIsBusy() && transition.getCurrentDuration() > 0){
                System.out.print("Transition " + transition.getUuid() + " busy ");
                System.out.println(transition.getCurrentDuration());
                // Transition is executing
                transition.countDown();
            } else if(transition.getIsBusy() && transition.getCurrentDuration() == 0){
                System.out.println("Transition " + transition.getUuid() + " endable");

                endableTransitionList.add(transition);
            }
            else {
                System.out.println("Transition " + transition.getUuid() + " not executable!");
                notExecutable += 1;
            }
        }

        if(notExecutable >= transitionList.size()){
            System.out.println("END");
            this.printer.endWriting();
            return false;
        }

        executableTransitionList = checkConflicts(executableTransitionList);

        this.startTransitions(executableTransitionList);
        this.endTransitions(endableTransitionList);
        return true;
    }

    private ArrayList<Transition> checkConflicts(ArrayList<Transition> executableTransitions){
        ArrayList<Transition> finalExecutableTransitions = new ArrayList<>();
        if(executableTransitions.size() < 2)
            return executableTransitions;

        for (int i=0; i<executableTransitions.size() - 1; i++){
            for(Location location: executableTransitions.get(i).getPrevLocationList()){
                for(int j=i+1; j<executableTransitions.size(); j++){
                    if(i==j) continue;
                    if(executableTransitions.get(j).getPrevLocationList().contains(location) && location.getTokens()==1){
                        System.out.print("Found Conflict between: ");
                        System.out.println(String.format("%s %s",executableTransitions.get(i).getUuid(), executableTransitions.get(j).getUuid()));
                        // Decide either by duration or random which transition will be executed
                        finalExecutableTransitions.add(solveConflict(executableTransitions.get(i), executableTransitions.get(j)));
                    }
                    else
                        finalExecutableTransitions.add(executableTransitions.get(i));
                }
            }
        }
        return finalExecutableTransitions;
    }

    private Transition solveConflict(Transition firstTransition, Transition secondTransition) {
        firstTransition.setNewDuration();
        secondTransition.setNewDuration();

        System.out.println(firstTransition.getUuid() + " - " + firstTransition.getCurrentDuration());
        System.out.println(secondTransition.getUuid() + " - " + secondTransition.getCurrentDuration());

        if(firstTransition.getCurrentDuration() < secondTransition.getCurrentDuration())
            return firstTransition;
        else if(firstTransition.getCurrentDuration() > secondTransition.getCurrentDuration())
            return firstTransition;
        else
            return (Math.random() <= 0.5) ? firstTransition : secondTransition;
    }

    private void startTransitions(ArrayList<Transition> transitionList){
        // This method will retrieve all the tokens from prev. locations, set executionTime on transition and set busy state
        for(Transition transition: transitionList) {
            for (Location location : transition.getPrevLocationList()) {
                location.setTokens(location.getTokens() - 1);
            }
            if(transition.getCurrentDuration() <= 0)
                transition.setNewDuration();
            transition.setIsBusy();
            if (transition.getCurrentDuration() > 0)
                transition.countDown();
        }
    }

    private void endTransitions(ArrayList<Transition> transitionList){
        // This method will place tokens in all the next (post) locations and reset busy state
        for(Transition transition: transitionList) {
            for (Location location : transition.getNextLocationList()) {
                location.setTokens(location.getTokens() + 1);
            }
            transition.resetIsBusy();
        }
    }

    private boolean transitionAvailable(Transition transition) {
        if(transition.getIsBusy()){
            return false;
        }
        for(Location prevLocation: transition.getPrevLocationList()){
            if(prevLocation.getTokens() == 0) return false;
        }
        return true;
    }

    private ArrayList<Integer> getCurrentMark(){
        ArrayList<Integer> currentMark = new ArrayList<>();
        for(Location location : this.locationList){
            currentMark.add(location.getTokens());
        }
        return currentMark;
    }

    private void print(int currentTick, ArrayList<Integer> mark){
        System.out.print("T" + currentTick + " : \t");
        for (Integer tokens : mark){
            System.out.print(tokens + "\t");
        }
        System.out.println();
    }
}
