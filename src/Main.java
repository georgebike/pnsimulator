import controller.SimulationController;
import models.Location;
import models.Transition;
import utils.Loader;
import utils.Printer;

import java.nio.file.Paths;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Loader loader = Loader.getInstance(Paths.get(System.getProperty("user.dir"), "data", "input2.json"));
        Printer printer = Printer.getInstance(Paths.get(System.getProperty("user.dir"), "data", "output.txt"));

        ArrayList<Location> locationList = loader.getLocations();
        ArrayList<Transition> transitionList = loader.getTransitions(locationList);

        SimulationController simController = new SimulationController(30, locationList, transitionList, printer);
//        SimulationController simController = new SimulationController(locationList, transitionList, printer);
        simController.commenceSimulation();
    }
}
